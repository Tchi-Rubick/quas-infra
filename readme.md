# Installation

Clone the following on the root of this project.

[quas-app](https://bitbucket.org/Tchi-Rubick/quas-app)

[quas-kit](https://bitbucket.org/Tchi-Rubick/quas-kit)


## Docker

Run the following commande.  
We assume that you have docker installed.

`docker compose up -d --build`

### Move into the container

quas-app: `docker exec -it quas-app sh`

quas-kit: `docker exec -it quas-kit sh`
